#!/usr/bin/make -f
# -*- makefile -*-

# Uncomment this to turn on verbose mode.
export DH_VERBOSE=1

DEB_HOST_MULTIARCH ?= $(shell dpkg-architecture -qDEB_HOST_MULTIARCH)
LIBDIR:=/usr/lib/$(DEB_HOST_MULTIARCH)

include /usr/share/mpi-default-dev/debian_defaults
MPI:=$(LIBDIR)/$(ARCH_DEFAULT_MPI_IMPL)

export HIPCXX=clang++-19

export MPICC=mpicc

ARCH_HIP:= amd64 arm64 ppc64el
HIP:= $(if $(filter $(DEB_TARGET_ARCH), $(ARCH_HIP)),--with-rocm=/usr,)

%:
	dh $@ 

# No ibverbs support available on kFreeBSD, Hurd
NO_VERBS_ARCH:= hurd-amd64 s390x
NO_JAVA_ARCH:= 
NO_NUMA_ARCH:= 

VERBS:=   $(if $(filter $(DEB_TARGET_ARCH), $(NO_VERBS_ARCH)), , --with-verbs )
NUMA:=   $(if $(filter $(DEB_TARGET_ARCH), $(NO_NUMA_ARCH)),  --disable-numa , )
ifeq ($(filter stage1,$(DEB_BUILD_PROFILES)),)
       JAVA := $(if $(filter $(DEB_TARGET_ARCH), $(NO_JAVA_ARCH)), \
                 ,--with-jdk-dir=/usr/lib/jvm/default-java --with-java=/usr/lib/jvm/default-java )
endif


override_dh_auto_configure:
	CPPFLAGS="-I/usr/include/hsa -I/usr/include/hip" dh_auto_configure --  \
		--enable-mt \
		--with-mpi=/usr \
		$(VERBS) \
		$(NUMA) \
		$(HIP) \
		--with-go=yes \
		--with-mad=yes \
		--enable-backtrace-detail \
		--enable-logging \
		--enable-devel-headers \
		--enable-examples \
		--enable-cma \
		--enable-lcov \
		--enable-profiling \
               --enable-rc \
               --enable-dc \
               --enable-ud \
               --with-mlx5-dv \
               --enable-dm

override_dh_auto_clean:
	dh_auto_clean
	find  . -type l -delete
	rm -f config.log

override_dh_shlibdeps:
	dh_shlibdeps --dpkg-shlibdeps-params=--ignore-missing-info

override_dh_auto_install:
	dh_auto_install
	chrpath -d debian/tmp/usr/lib/*/lib*.so.0*

override_dh_dwz:
	@echo "pass; dwz currently breaks"
